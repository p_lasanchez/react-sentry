import React, { useState, memo } from 'react'
import { GlobalStyle } from './GlobalStyle'

import logo from './assets/logo.png'
import { images } from './assets/images'
import { SCHeader, SCLogo, SCMain, SCImage } from './styled/styled'
import { ErrorBoundary } from './components/Boundary'

const Header: React.FC = () => (
  <SCHeader>
    <SCLogo src={logo} alt="logo" />
  </SCHeader>
)

const Image: React.FC<{ image: string }> = memo(({ image }) => {
  const [light, setLight] = useState<boolean>(false)
  const handleClick = (evt: any) => {
    setLight(!light)
    evt.start()
  }

  return (
    <SCImage src={image} onClick={handleClick} light={light} />
  )
})

const Images: React.FC = () => (
  <SCMain>
    {
      images.map(image => <Image image={image} key={image} />)
    }
  </SCMain>
)

const App: React.FC = () => (
  <ErrorBoundary>
    <GlobalStyle />
    <Header />
    <Images />
  </ErrorBoundary>
)

export default App
