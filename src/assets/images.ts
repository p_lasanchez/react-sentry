export const images: string[] = [
  'https://s3-us-west-2.amazonaws.com/artifactuprising/magento/blog/articles/2016-06-27-black-white-photo/2016-06-27-black-white-photo-01.jpg',
  'https://s3-us-west-2.amazonaws.com/artifactuprising/magento/blog/articles/2016-06-27-black-white-photo/2016-06-27-black-white-photo-02.jpg',
  'https://s3-us-west-2.amazonaws.com/artifactuprising/magento/blog/articles/2016-06-27-black-white-photo/2016-06-27-black-white-photo-03.jpg',
  'https://s3-us-west-2.amazonaws.com/artifactuprising/magento/blog/articles/2016-06-27-black-white-photo/2016-06-27-black-white-photo-04.jpg',
  'https://s3-us-west-2.amazonaws.com/artifactuprising/magento/blog/articles/2016-06-27-black-white-photo/2016-06-27-black-white-photo-05.jpg',
  'https://s3-us-west-2.amazonaws.com/artifactuprising/magento/blog/articles/2016-06-27-black-white-photo/2016-06-27-black-white-photo-00.jpg',
  'https://s3-us-west-2.amazonaws.com/artifactuprising/magento/blog/articles/2016-06-27-black-white-photo/2016-06-27-black-white-photo-06.jpg',
  'https://s3-us-west-2.amazonaws.com/artifactuprising/magento/blog/articles/2016-06-27-black-white-photo/2016-06-27-black-white-photo-08.jpg',
  'https://s3-us-west-2.amazonaws.com/artifactuprising/magento/blog/articles/2016-06-27-black-white-photo/2016-06-27-black-white-photo-09.jpg',
  'https://s3-us-west-2.amazonaws.com/artifactuprising/magento/blog/articles/2016-06-27-black-white-photo/2016-06-27-black-white-photo-11.jpg',
  'https://google.fr/image.png'
]
