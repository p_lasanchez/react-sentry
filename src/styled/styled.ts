import styled from 'styled-components'

export const SCHeader = styled.header`
  background: #16111C;
  padding: 2rem;
  width: 100%;
` 

export const SCLogo = styled.img`
  width: auto;
  height: 50px;
`

export const SCImage = styled.article<{ src: string, light: boolean }>`
  background: url(${props => props.src}) center center;
  background-size: cover;
  border-radius: 0.5rem;
  border-style: solid;
  border-width: ${props => props.light ? '5px' : 0};
  border-color: #E03E2F;
  cursor: pointer;
  width: 200px;
  height: 200px;
  margin: 0 50px 50px 0;
  &:last-of-type {
    margin-right: auto;
  }
`

export const SCMain = styled.main`
  color: white;
  display: flex;
  flex-wrap: wrap;
  margin: 0 auto;
  padding: 2rem 0;
  width: 750px;
`
