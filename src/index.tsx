import 'react-app-polyfill/ie11'
import React from 'react';
import ReactDOM from 'react-dom';
import * as Sentry from '@sentry/browser'

import App from './App';
import * as serviceWorker from './serviceWorker';

Sentry.init({
  release: 'react-test-007',
  dsn: "https://4f1308690d514253bc70b150b07c8f5a@sentry.io/1821948",
  beforeSend(event, hint) {
    // Check if it is an exception, and if so, show the report dialog
    if (event.exception) {
      Sentry.showReportDialog({ eventId: event.event_id });
    }
    return event;
  }
})
ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
