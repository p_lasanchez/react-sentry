import React, { Component } from 'react';
import * as Sentry from '@sentry/browser';

export class ErrorBoundary extends Component<{}, { eventId: string | null, hasError: boolean }> {
    constructor(props: any) {
        super(props);
        this.state = { eventId: null, hasError: false }
    }

    static getDerivedStateFromError(error: any) {
      return { hasError: true };
    }

    componentDidCatch(error: any, errorInfo: any) {
      Sentry.withScope((scope) => {
          scope.setExtras(errorInfo);
          const eventId = Sentry.captureException(error);
          this.setState({eventId});
      });
    }

    render() {
        if (this.state.hasError) {
            //render fallback UI
            return (
              <button onClick={() => Sentry.showReportDialog({ eventId: this.state.eventId } as Sentry.ReportDialogOptions)}>Report feedback</button>
            );
        }

        // when there's not an error, render children untouched
        return this.props.children;
    }
}
